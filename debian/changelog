libalias-perl (2.32-12) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Change bugtracker URL(s) to HTTPS.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Wrap long lines in changelog entries: 2.32-5, 2.32-3, 2.32-1.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 05 Jun 2022 18:55:48 +0100

libalias-perl (2.32-11) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Damyan Ivanov ]
  * mark package as autopkg-testable
  * Declare conformance with Policy 3.9.6

 -- Damyan Ivanov <dmn@debian.org>  Sat, 21 Nov 2015 19:56:32 +0000

libalias-perl (2.32-10) unstable; urgency=low

  * Team upload

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Florian Schlichting ]
  * Switch dh compatibility to level 9 to enable passing of hardening flags
  * Declare compliance with Debian Policy 3.9.5

 -- Florian Schlichting <fsfs@debian.org>  Fri, 07 Mar 2014 00:24:45 +0100

libalias-perl (2.32-9) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.

  [ Joachim Breitner ]
  * Removed myself from uploaders.

  [ gregor herrmann ]
  * debian/watch: use dist-based URL.
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ gregor herrmann ]
  * Change my email address.
  * Remove Conflicts:/Provides: alias.
  * Switch to source format 3.0 (quilt).
  * Add patch by Niko Tyni to work with perl 5.14 (closes: #627805).
  * Set Standards-Version to 3.9.2 (no further changes).
  * Bump debhelper compatibility level to 8.
  * Use tiny debian/rules.
  * debian/control: improve short and long description.
  * debian/copyright: convert to DEP5 format.
  * debian/control: remove Section and Priority from binary package stanza,
    thanks lintian.

 -- gregor herrmann <gregoa@debian.org>  Sun, 10 Jul 2011 21:13:33 +0200

libalias-perl (2.32-8) unstable; urgency=low

  * debian/control: Standards-Version bumped to 3.6.2 without changes
  * Add a watch file to the package

 -- Joachim Breitner <nomeata@debian.org>  Fri,  8 Oct 2004 12:05:55 +0200

libalias-perl (2.32-7) unstable; urgency=low

  * Adopted by Debian Perl Group (Closes: 274123)

 -- Joachim Breitner <nomeata@debian.org>  Fri,  8 Oct 2004 11:48:57 +0200

libalias-perl (2.32-6) unstable; urgency=low

  * Add Section and Priority to the control file.
  * Update standards version.

 -- Michael Alan Dorman <mdorman@debian.org>  Fri, 19 Dec 2003 15:09:00 -0500

libalias-perl (2.32-5) unstable; urgency=low

  * Fix copyright file to reference copies of the pertinent licenses (closes:
    Bug#157539)
  * Acknowledge NMU (closes: Bug#158559)
  * Retain the ability to build on woody
  * Move to debhelper 4
  * Fix section

 -- Michael Alan Dorman <mdorman@debian.org>  Tue, 16 Dec 2003 08:22:49 -0500

libalias-perl (2.32-4.1) unstable; urgency=low

  * NMU for perl 5.8.
  * Build for perlapi-5.8.0 and update perl build-dependency to >= 5.8.0-7.

 -- Colin Watson <cjwatson@debian.org>  Wed, 28 Aug 2002 00:30:23 +0100

libalias-perl (2.32-4) unstable; urgency=low

  * Built to comply with latest perl policy and standards.

 -- Michael Alan Dorman <mdorman@debian.org>  Sun, 15 Apr 2001 14:53:02 -0400

libalias-perl (2.32-3) unstable; urgency=low

  * Too stupid to get the binary-arch stuff in the right place.  (closes:
    bug#40969)

 -- Michael Alan Dorman <mdorman@debian.org>  Sun, 11 Jul 1999 12:51:53 -0400

libalias-perl (2.32-2) unstable; urgency=low

  * Modified for new perl packages.
  * Modified to use debhelper.

 -- Michael Alan Dorman <mdorman@debian.org>  Mon,  5 Jul 1999 17:01:32 +0000

libalias-perl (2.32-1) unstable; urgency=low

  * New upstream version.
  * Changed package name to libalias-perl to be consistent with other perl
    packages.

 -- Michael Alan Dorman <mdorman@debian.org>  Sat, 12 Jun 1999 17:28:45 -0400

alias (2.31-2) unstable; urgency=low

  * Remember to build with library dependencies

 -- Michael Alan Dorman <mdorman@debian.org>  Fri, 05 Feb 1999 16:07:20 -0500

alias (2.31-1) unstable; urgency=low closes=18871 16748 30062

  * New upstream release
  * Updated copyright file
  * Updated build procedure (closes: bug#30062)
  * Corrected spelling in description (closes: bug#18871)
  * Corrected standards version (closes: bug#16748)

 -- Michael Alan Dorman <mdorman@debian.org>  Sun, 31 Jan 1999 16:07:20 -0500

alias (2.3-1) unstable; urgency=low

  * New upstream release
  * Make dependency on perl correct

 -- Michael Alan Dorman <mdorman@debian.org>  Sat, 8 Mar 1997 13:52:47 -0500

alias (2.1-3) unstable; urgency=low

  * Corrected rules arch-indep/dep distribution (Bug#4629)
  * Make sure shared library is stripped (Bug#5122)

 -- Michael Alan Dorman <mdorman@debian.org>  Wed, 13 Nov 1996 13:26:56 -0500

alias (2.1-2) unstable; urgency=low

  * Corrected source directory name so dpkg-source doesn't barf
  * Corrected maintainer name

 -- Michael Alan Dorman <mdorman@debian.org>  Wed, 13 Nov 1996 13:19:23 -0500

alias (2.1-1) unstable; urgency=low

  * Initial packaging.

 -- Michael Alan Dorman <mdorman@calder.med.miami.edu>  Fri, 13 Sep 1996 10:26:20 -0400
